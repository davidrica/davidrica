import React, { Component } from "react";
import { visa, visa3, mastercard, mastercard3, jcb, jcb3, amex, amex3 } from '../../common/staticImage';

class Payment extends Component {
    render() {
        return (
            <div className="airline-payment">
                <div className="header">
                    <div className="top-header">
                        <h3>Please select your payment method</h3>
                    </div>
                    <div className="bottom-header">
                        <p className="fs-13">Total to be paid by Agent</p>
                        <h1>IDR 11,450,000.-</h1>
                    </div>
                </div>
                <div className="description">
                    <p>You will be redirected to an external page when you click on the confirm trip button.</p>
                    <p>The methods of payment available will include:</p>
                </div>
                <div className="content">
                    <div className="tabs">
                        <ul className="flex-between">
                            <li className="inline-block active">Credit Card</li>
                            <li className="inline-block">Internet Banking</li>
                            <li className="inline-block">Debit Card</li>
                            <li className="inline-block">Other Payment</li>
                        </ul>
                    </div>
                    <div className="payment-form clearfix">
                        <div className="float-left form-wrapper">
                            <div className="input-group">
                                <p>Card Holder Name</p>
                                <input type="text"/>
                            </div>
                            <div className="input-group">
                                <p>Card Number</p>
                                <input type="text"/>
                            </div>
                            <div className="input-group-inline flex-between">
                                <div className="input-group inline-block">
                                    <p>Expired Date</p>
                                    <input type="text" placeholder="MM/YY"/>
                                </div>
                                <div className="input-group inline-block">
                                    <p>CCV Code</p>
                                    <input type="text" placeholder="CCV"/>
                                </div>
                            </div>
                            <div className="input-group">
                                <p>Card Issuer</p>
                                <input type="text"/>
                            </div>
                            <div className="input-group">
                                <p>Card Issuer Country</p>
                                <input type="text"/>
                            </div>
                        </div>
                        <div className="float-right payment-list">
                            <img src={visa} srcSet={visa3} width={88} alt="Visa"/>
                            <img src={mastercard} srcSet={mastercard3} width={72} alt="Mastercard"/>
                            <img src={jcb} srcSet={jcb3} width={65} alt="Job"/>
                            <img src={amex} srcSet={amex3} width={69} alt="Amex"/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Payment;
