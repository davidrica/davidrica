import React, { Component } from "react";
import { caretDownThin, caretDownThin3, planeRight, planeRight3, planeLeft, planeLeft3} from '../../common/staticImage'

class Summary extends Component {
    render() {
        return (
            <div className="airline-summary">
                <div className="summary-header">
                    <h2>Summary</h2>
                </div>
                <div className="summary-body">
                    <div className="departure clearfix">
                        <div className="header clearfix">
                            <p className="float-left">Departure</p>
                            <img className="float-right" src={caretDownThin} srcSet={caretDownThin3} width={13} alt="Caret Down"/>
                        </div>
                        <div className="body">
                            <img className="inline-block" src={planeRight} srcSet={planeRight3} width={17} alt="Caret Down"/>
                            <div className="inline-block text-wrapper">
                                <p className="bold">GA120  ( CGK - SIN )</p>
                                <p>Sat, 2 December 2017 - 14:00 Economy Class (B)</p>
                            </div>
                        </div>
                        <hr/>
                        <div className="footer">
                            <p className="bold">Price</p>
                            <div className="subtotal flex-between">
                                <p>Sub Total Price</p>
                                <p className="bold">5,375,000</p>
                            </div>
                        </div>
                        <a href="" className="float-right clearfix">Details</a>
                    </div>
                    <hr/>
                    <div className="return clearfix">
                        <div className="header clearfix">
                            <p className="float-left">Return</p>
                            <img className="float-right" src={caretDownThin} srcSet={caretDownThin3} width={13} alt="Caret Down"/>
                        </div>
                        <div className="body">
                            <img className="inline-block" src={planeLeft} srcSet={planeLeft3} width={17} alt="Caret Down"/>
                            <div className="inline-block text-wrapper">
                                <p className="bold">GA120  ( SIN - CGK )</p>
                                <p>Tue, 5 December 2017 - 14:00 Economy Class (Q)</p>
                            </div>
                        </div>
                        <hr/>
                        <div className="footer">
                            <p className="bold">Price</p>
                            <div className="subtotal flex-between">
                                <p>Sub Total Price</p>
                                <p className="bold">6,075,000</p>
                            </div>
                        </div>
                        <a href="" className="float-right clearfix">Details</a>
                    </div>
                </div>
                <div className="summary-footer flex-between">
                    <p className="bold">Grand Total</p>
                    <p className="bold">IDR 11,450,000</p>
                </div>
            </div>
        );
    }
}

export default Summary;
