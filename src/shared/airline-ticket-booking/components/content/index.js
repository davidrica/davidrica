import React, { Component } from "react";
import Passenger from './Passenger';
import Payment from './Payment';
import Summary from './Summary';


class Content extends Component {
    render() {
        const { step, changeStep } = this.props;
        return (
            <div className="airline-content clearfix">
                <div className="container">
                    {
                        step === 'passenger' ?
                            <div className="content-header">
                                <h2 className="inline">Passenger Information</h2>
                                <p className="fs-13 inline">Name as on ID Card / Passport / Driving Lisence</p>
                            </div> :
                            <div className="content-header">
                                <h2 className="inline">Payment</h2>
                            </div>
                    }

                    <div className="float-left">
                        {
                            step === 'passenger' ?
                                <div>
                                    <Passenger/>
                                    <button onClick={changeStep}>CONTINUE TO PAYMENT</button>
                                </div> :
                                <div>
                                    <Payment/>
                                    <button className="disabled" onClick={changeStep}>CONTINUE TO CONFIRMATION</button>
                                </div>
                        }
                    </div>
                    <div className="float-right">
                        <Summary/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Content;
