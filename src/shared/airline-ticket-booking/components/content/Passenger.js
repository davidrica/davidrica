import React, { Component } from "react";
import { caretDownB,caretDownB3 } from '../../common/staticImage';

class Passenger extends Component {
    render() {
        const passengerList = ['1', '2', '3'];
        return (
            <div className="airline-passenger">
                {passengerList.map((passenger, i)=> {
                    return (
                        <div className="passenger" key={i}>
                            <div className="header">
                                <h3>Passenger {passenger}</h3>
                            </div>
                            <div className="content flex-between flex-wrap">
                                <div className="input-group select-group">
                                    <p>Title</p>
                                    <select className="small" name="title" id="title">
                                        <option value="male">Mr</option>
                                        <option value="female">Ms</option>
                                    </select>
                                    <img src={caretDownB} alt="Caret Down"/>
                                </div>

                                <div className="input-group">
                                    <p>First Name</p>
                                    <input className="medium" type="text" placeholder="First Name"/>
                                </div>

                                <div className="input-group">
                                    <p>Middle Name</p>
                                    <input className="medium" type="text" placeholder="Middle Name"/>
                                </div>

                                <div className="input-group">
                                    <p>Last Name</p>
                                    <input className="medium" type="text" placeholder="Last Name"/>
                                </div>

                                <div className="input-group">
                                    <p>ID Number</p>
                                    <input className="large" type="text" placeholder="ID Number"/>
                                </div>

                                <div className="input-group">
                                    <p>Phone</p>
                                    <input className="large" type="text" placeholder="Phone"/>
                                </div>

                                <div className="input-group select-group">
                                    <p>Remark</p>
                                    <select className="large" name="remark" id="remark">
                                        <option value="contact-person">Set as contact person</option>
                                    </select>
                                    <img src={caretDownB} alt="Caret Down"/>
                                </div>

                            </div>
                        </div>
                    )
                })}
            </div>
        );
    }
}

export default Passenger;
