import React, { Component } from "react";
import {
    garudaLogo, garudaLogo3, userIcon, userIcon3, caretDownA, caretDownA3, globe, globe3, home, home3,
    bottomHeaderBackground, bottomHeaderBackground3, caretRightThin, caretRightThin3, divider, divider3, dividerBlue,
    dividerBlue3
} from '../common/staticImage'

class Header extends Component {
    render() {
        const { step } = this.props;
        const navigationList = ["Reservation", "Transaction Review", "Management Booker", "Profil Gos", "Referensi"];
        return (
            <div className="airline-header">
                <div className="top-header">
                    <div className="container">
                        <img className="inline-block float-left" src={garudaLogo} srcSet={garudaLogo3} alt="Garuda Indonesia Logo" width="224"/>
                        <ul className="inline-block float-right mv-0 ph-0">
                            {navigationList.map((navItem, i)=> {
                                const navItemClass = navItem === 'Reservation' ? 'inline active' : 'inline';
                                return <li className={navItemClass} key={i}><a href="">{navItem}</a></li> })}
                            <li className="user-navigation inline">
                                <img src={userIcon} srcSet={userIcon3} width="13" alt="User"/>
                                <a href="">Welcome, David</a>
                                <img src={caretDownA} srcSet={caretDownA3} width="8" alt="Caret Down"/>
                            </li>
                            <li className="inline language">
                                <img src={globe} srcSet={globe3} width="16" alt="Globe"/>
                                <a href="">ID</a>
                                <img src={caretDownA} srcSet={caretDownA3} width="8" alt="Caret Down"/>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="bottom-header">
                    <div className="container">
                        <ul className="inline-block">
                            <li className="inline"><img src={home} srcSet={home3} width="10" alt="Home"/></li>
                            <li className="inline"><img src={caretRightThin} srcSet={caretRightThin3} width="7" alt="Caret Right"/></li>
                            <li className="inline">Reservation</li>
                            <li className="inline"><img src={caretRightThin} srcSet={caretRightThin3} width="7" alt="Caret Right"/></li>
                            <li className="inline">Search Flight</li>
                        </ul>
                    </div>
                    <img className="float-right inline-block bottom-header-background" src={bottomHeaderBackground}
                         srcSet={bottomHeaderBackground3} width={842} alt="background"/>
                </div>
                <div className="step-header">
                    <div className="container">
                        <div className="step-group inline-block">
                            <div className="step active">
                                <div className="step-round">1</div>
                                <span>Passenger</span>
                                <img className="inline-block"
                                     src={step === 'passenger' ? divider : dividerBlue}
                                     srcSet={step === 'passenger' ? divider3 : dividerBlue3} width={186} alt="divider"/>
                            </div>
                            <div className={step === 'passenger' ? 'step' : 'step active'}>
                                <div className="step-round">2</div>
                                <span>Payment</span>
                                <img className="inline-block" src={divider} srcSet={divider3} width={186} alt="divider"/>
                            </div>
                            <div className="step">
                                <div className="step-round">3</div>
                                <span>Confirmation</span>
                            </div>
                        </div>
                        <button className="float-right">CHANGE FLIGHT</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Header;
