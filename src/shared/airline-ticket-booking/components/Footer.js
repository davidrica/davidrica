import React, { Component } from "react";

class Footer extends Component {
    render() {
        const year = new Date().getFullYear();
        return (
            <div className="airline-footer">
                <div className="container">
                    <p>Copyrights © {year} Garuda Indonesia. All rights reserved.</p>
                </div>
            </div>
        );
    }
}

export default Footer;
