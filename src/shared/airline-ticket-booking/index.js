import React, { Component } from 'react';
import Header from './components/Header';
import './styles/sass/style.scss';
import Footer from './components/Footer';
import Content from './components/content';

class AirlineTicketBooking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            step: 'passenger'
        };
    }
    componentDidMount() {
        document.title = "Airline Ticket Booking";
    }
    changeStep = () => {
        this.setState({
            step: this.state.step === 'payment' ? 'passenger' : 'payment'
        });
    };
    render() {
        console.log(" Developed by David Ricardo \n Website: https://davidrica.com \n Email: davidlikaldo@gmail.com");
        const { step } = this.state;
        return (
            <div className="airline-ticket-booking">
                <Header step={step} />
                <Content step={step} changeStep={this.changeStep} />
                <Footer/>
            </div>
        );
    }
}

export default AirlineTicketBooking;
