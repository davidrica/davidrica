import React, { Component } from 'react'
import Header from './Header';
import Footer from './Footer';

class Main extends Component {
	render() {
		return (
			<div>
				<Header/>
				{React.cloneElement(this.props.children, this.props)}
				<Footer/>
			</div>
		)
	}
}

export default Main;
