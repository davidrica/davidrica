import React, { Component } from 'react'
import footerLogo from '../images/FooterLogo.png';

class Footer extends Component {
	render() {
		return (
			<div className="footer">
				<div className="footer-top">
					<div className="container">
						<div className="ft-left">
							<ul>
								<li className="active"><a>Bahasa Indonesia</a></li>
								<li><a>English</a></li>
							</ul>
						</div>
						<div className="ft-right">
							<ul>
								<li><a>Rates</a></li>
								<li><a>Terms & Conditions</a></li>
								<li><a>Privacy Policy</a></li>
								<li><a>Help</a></li>
								<li><a>Contact Us</a></li>
							</ul>
						</div>
						<div className="clearfix"></div>
					</div>
				</div>
				<div className="footer-bottom">
					<div className="container">
						<div className="footer-logo">
							<img src={footerLogo}/>
						</div>
						<div className="fb-text">
							<p>MyBank Internet Business is Service mark of PT.MyBank, Tbk</p>
							<p>Lorem ipsum dolor sit amet consectetur adipiscing elit sed do.</p>
						</div>
					</div>
				</div>
				<div className="clearfix"></div>
			</div>
		)
	}
}

export default Footer;
