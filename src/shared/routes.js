import Home from './home/Home';
import News from './news/News';
import FinancialDashboard from './financial-dashboard/components/Home';
import AirlineTicketBooking from './airline-ticket-booking/index';

const routes = [
  {
    path: '/',
    exact: true,
    component: Home
  },
  {
    path: '/news',
    component: News
  },
  {
    path: '/financial-dashboard',
    component: FinancialDashboard
  },
  {
    path: '/airline-ticket-booking',
    component: AirlineTicketBooking
  },
];

export default routes;
