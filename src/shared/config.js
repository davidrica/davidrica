// export const ENV = 'DEV';
export const ENV = 'PROD';
export const SERVER_HOST = ENV === 'PROD' ? 'https://davidrica.com' : 'http://127.0.0.1:3000';

